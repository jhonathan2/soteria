var Usuario;

var DisponibilidadPreguntas = 0;
var Municipios;
var Reporte1_Items;
var Reporte1_SubItems;

var Reporte1_Item;
var Reporte1_SubItem;
var Reporte1_Tarjeta = 'Ninguna';
//var Reporte_Regional = 0;
//var Reporte_Area = 0;
//var Reporte_Departamento = 0;
//var Reporte_Municipio = 0;
//var Reporte_Proyecto = 0;
//var Reporte_AreaCierre = 0;
var Reporte_CausaRaiz = 0;

var filtrosArray = new Array();



$(document).on("ready", arranque);

function arranque()
{
	crearTabla($("#ActDiligenciados_Table"), "CTW");
	$(".btnActos_Estado").hover(function()
		{
			var obj = $(this).find('.btnActos_Estado_Tooltip');
			$(obj).show("slide");

		}, function()
		{
			var obj = $(this).find('.btnActos_Estado_Tooltip');
			$(obj).hide("slide");
		});
	$(".btnActos_Estado").on("click", function()
	{
		if ($(this).attr("Valor") == 0)
		{
			$(this).css("background", "blue");
			$(this).css("color", "white");
			$(this).attr("Valor", 1);
		} else
		{
			$(this).css("background", "none");
			$(this).css("color", "black");
			$(this).attr("Valor", 0);
		}
	});
	CargarUsuario();
	$('button').button();
	
	$("#btnEditarUsuario_CambiarClave").on("click", btnEditarUsuario_CambiarClave_Click);	

	$("#Dashboard_Condiciones").on("click", function()
	{

			Seccion("#CrearReporte"); 
			$("#SelectedSection h4").text("Reportar condiciones inseguras");
	});

	$("#Dashboard_Actos").on("click", function()
	{

			Seccion("#CrearActo"); 
			$("#SelectedSection h4").text("Reportar Actos inseguros");
	});

	$(".MainMenu_Item").on('click', 
		function()
		{ 
			var IdSeccion = $(this).attr("id").replace("lnk", "");
			Seccion("#" + IdSeccion); 
			$("#SelectedSection h4").text($("#" + IdSeccion).attr("Texto"));
		});
	$('#lnkLogout').on('click', CerrarSesion);

	$("#cboReporte_Proyecto").on("change", function()
	{
		if ($(this).val() == 1)
		{
			$("#Reporte_CrearProyecto").slideDown();
			$("#txtReporte_NuevoProyecto").focus();
		} else
		{
			btnReporte_CrearProyecto_Cancelar_Click(event);
		}
	});

	$("#cboActo_Proyecto").on("change", function()
	{
		if ($(this).val() == 1)
		{
			$("#Acto_CrearProyecto").slideDown();
			$("#txtActo_NuevoProyecto").focus();
		} else
		{
			btnActo_CrearProyecto_Cancelar_Click(event);
		}
	});

	$("#cboReporte_Departamento").on("change", function()
	{	
		cargarMunicipios($(this).val(), $("#cboReporte_Municipio"));
	});

	$("#cboActo_Departamento").on("change", function()
	{	
		cargarMunicipios($(this).val(), $("#cboActo_Municipio"));
	});
	
	ArranquePropio();
}
function CargarUsuario()
{
	if(!localStorage.UsuarioWSPSISO)
	{CerrarSesion();}
	Usuario = JSON.parse(localStorage.UsuarioWSPSISO)[0];
	$("#lblWelcome span").text(Usuario.Nombre);
	$("#lblWelcomeRol span").text(Usuario.Correo);
	$("#lblPolla_Concursante small").text(Usuario.Nombre);
}
function CerrarSesion()
{
	delete localStorage.UsuarioWSPSISO;
	window.location.replace("index.html");
}
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function DashBoard_RangeBx_Change()
{
	$(this).val(Date.parse($(this).val()).toString("yyyy-MM-dd"));
}
function MostrarAlerta(TipoMensaje, Icono, Strong, Mensaje)
{
	/*NombreContenedor : Id del Div que contiene el MessageAlert
	 * TipoMensaje : {highlight, error, default}
	 * Icono : Icono que acompaña el mensaje ver listado en bootstrap
	 * Mensaje del AlertMessage*/
	 
	$("#alertMensaje").removeClass(function() {return $(this).prev().attr('class');});
	$("#alertMensaje").addClass("Alerta");
	$("#alertMensaje span").removeClass("*");
	$("#alertMensaje").addClass("ui-state-" + TipoMensaje);
	$("#alertMensaje span").addClass(Icono);
	$("#alertMensaje strong").text(Strong);
	$("#alertMensaje texto").text(Mensaje);
	
	$("#alertMensaje").fadeIn(300).delay(2600).fadeOut(600);
}
function obtenerFecha()
{
	var f = new Date();
	return f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2) ;
}
function obtenerHora()
{
	var f = new Date();
	return CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2) +":" +  CompletarConCero(f.getSeconds(), 2);
}
function OcultarMenu()
{
	if ($("#MainMenu_Footer").is (':visible'))
	{
			$(".Seccion").addClass("Seccion_SinMenu");
			$(".Seccion").removeClass("Seccion");

			$("#MainMenu").hide('slide', 50);
	}
	else
	{
		$(".Seccion_SinMenu").addClass("Seccion");
		$(".Seccion_SinMenu").removeClass("Seccion_SinMenu");

		$("#MainMenu").show('slide', 50);
	}
}
function Seccion(obj)
{
	if (screen.width<769)
		{OcultarMenu();}
	
	$(".Seccion").fadeOut();
	$(".Seccion_SinMenu").fadeOut();

	$(obj).fadeIn();
}
/*
(function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
          	change: function(event, ui)
          	{
          		var idElemento = $(ui.item.option.parentElement).attr("id");

          		if (idElemento == "cboReporte_Area")
	          		{
	          			Reporte_Area = ui.item.option.value;;
	          		}
          		if (idElemento == "cboReporte_Departamento") 
          			{
          				cargarMunicipios(ui.item.option.value, $("#cboReporte_Municipio"));
          				Reporte_Departamento = ui.item.option.value;
          			}
          		if (idElemento == "cboReporte_Municipio")
	          		{
	          			Reporte_Municipio = ui.item.option.value;
	          		}
	          	if (idElemento == "cboReporte_Regional")
		          	{
		          		Reporte_Regional = ui.item.option.value;
		          	}
	          	if (idElemento == "cboReporte_Responsable")
		          	{
		          		Reporte_AreaCierre = ui.item.option.value;
		          	}
          		if (idElemento == "cboReporte_Proyecto") 
          			{
          				if (ui.item.option.value == 1)
          				{
          					$("#Reporte_CrearProyecto").slideDown();
          					$("#txtReporte_NuevoProyecto").focus();
          				} else
          				{
          					btnReporte_CrearProyecto_Cancelar_Click(event);
          					Reporte_Proyecto = ui.item.option.value;
          				}
          			}
          	},
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          });
 
        
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Mostrar Todos" )
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );
 
/************************Codigo Privado**********************************/
function ArranquePropio()
{
	CargarPermisos();
	var f = new Date();

	if (Usuario.Rol == 1)
	{
		$("#frmUsuarios_Buscar").on("submit", CargarUsuarios);
		$("#btnGraficas_Buscar").on("click", verGrafica);

		$( "#Graficas_Parametros" ).sortable({
	      connectWith: "ul",
	      dropOnEmpty: true,
	      beforeStop: function( event, ui ) 
	      {
			graficas_CargarParametros();
	      }
	  	});

	  	$( "#Graficas_ejeX" ).sortable({
	      connectWith: "ul",
	      dropOnEmpty: true,
	      beforeStop: function( event, ui ) 
	      {
	      	graficas_CargarParametros();
	      }
	  	});
		
		$("#txtGraficas_Desde").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-01")
		$("#txtGraficas_Hasta").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));

		$("#txtRepDiligenciados_Desde").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-01")
		$("#txtRepDiligenciados_Hasta").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));

		$("#txtActDiligenciados_Desde").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-01")
		$("#txtActDiligenciados_Hasta").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));

		$("#btnRepDiligenciados_Detalles").on("click", function()
			{
				$("#RepDiligenciados_Estados").hide();
				$("#RepDiligenciados_Detalles").show("slide");
			});
		$("#btnRepDiligenciados_Estados").on("click", btnRepDiligenciados_Detalles_Click);
		$("#btnRepDiligenciados_CambiarArea").on("click", btnRepDiligenciados_CambiarArea_Click);

		//CargarUsuarios();
	}

	
	
	$(".artReporte_Actos_Item").live("click", artReporte_Actos_Item_Click);
	$(".artReporte_Actos_SubItem").live("click", artReporte_Actos_SubItem_Click);

	$("#btnReporte_CrearProyecto").on("click", btnReporte_CrearProyecto_Click);
	$("#btnReporte_CrearProyecto_Cancelar").on("click", btnReporte_CrearProyecto_Cancelar_Click);
	$("#btnActo_CrearProyecto").on("click", btnActo_CrearProyecto_Click);
	$("#btnActo_CrearProyecto_Cancelar").on("click", btnActo_CrearProyecto_Cancelar_Click);

	$("#btnReporte_Guardar").on("click", btnReporte_Guardar_Click);

	$("#btnUsuarios_Crear").on("click", btnUsuarios_Crear_Click);
	$(".btnUsuarios_Editar").live("click", btnUsuarios_Editar_Click);

	
	$("#CrearReporte_tabs").tabs();
	$("#CrearActo_tabs").tabs();

	//$(".Dashboard_frmComentar").live("submit", Dashboard_frmComentar_Submit);
	//$("#Dashboard_Publicar").on("submit", Dashboard_Publicar_Submit);

	$("#frmRep1").on("submit", frmRep1_Submit);
	$("#frmRep2").on("submit", frmRep2_Submit);

	$(".itemCausaRaiz").live("click", itemCausaRaiz_Click);

	$("#RepDiligenciados_Buscar").on("click", RepDiligenciados_Buscar_Click);
	$("#ActDiligenciados_Buscar").on("click", ActDiligenciados_Buscar_Click);
	$("#RepDiligenciados_Table tbody tr").live("click", RepDiligenciados_Table_tr_Click);
	$("#Reporte_Calificacion article").on("click", Reporte_Calificacion_article_click);

	$("#txtRepDiligenciados_Desde").datepicker();
	$("#txtRepDiligenciados_Desde").on('change', DashBoard_RangeBx_Change);

	$("#txtRepDiligenciados_Hasta").datepicker();
	$("#txtRepDiligenciados_Hasta").on('change', DashBoard_RangeBx_Change);

	$("#txtActDiligenciados_Desde").datepicker();
	$("#txtActDiligenciados_Desde").on('change', DashBoard_RangeBx_Change);

	$("#txtActDiligenciados_Hasta").datepicker();
	$("#txtActDiligenciados_Hasta").on('change', DashBoard_RangeBx_Change);

	$("#txtGraficas_Desde").datepicker();
	$("#txtGraficas_Desde").on('change', DashBoard_RangeBx_Change);

	$("#txtGraficas_Hasta").datepicker();
	$("#txtGraficas_Hasta").on('change', DashBoard_RangeBx_Change);
	
	$("#txtReporte_Fecha").datepicker();
	$("#txtReporte_Fecha").on('change', DashBoard_RangeBx_Change);

	$("#txtActo_Fecha").datepicker();
	$("#txtActo_Fecha").on('change', DashBoard_RangeBx_Change);
	
	$("#txtReporte_FechaCierre").datepicker();
	$("#txtReporte_FechaCierre").on('change', DashBoard_RangeBx_Change);

	$("#txtReporte_FechaCorreccion").datepicker();
	$("#txtReporte_FechaCorreccion").on('change', DashBoard_RangeBx_Change);
	
	//$("#txtUsuarios_Crear_Correo").on("change", txtUsuarios_Crear_Correo_Change);
	$("#txtUsuarios_Crear_Usuario").on("change", txtUsuarios_Crear_Usuario_Change);

	CargarAreas($("#cboReporte_Area"), 0);
//		$("#cboReporte_Area").combobox();
	CargarAreas($("#cboReporte_Responsable"), 1);

	CargarAreas($("#txtCambiarArea_Area"), 1);
//		$("#cboReporte_Responsable").combobox();
	CargarAreas($("#txtUsuarios_Area"), 0);
	CargarAreas($("#txtUsuarios_Crear_Area"), 0);

	CargarCausaRaiz();

	CargarDepartamentos($("#cboReporte_Departamento"));
//		$( "#cboReporte_Departamento" ).combobox();
	CargarProyectos($("#cboReporte_Proyecto"));
//		$("#cboReporte_Proyecto").combobox();
	
	CargarRegionales($("#cboReporte_Regional"));
//		$("#cboReporte_Regional").combobox();

	
	CargarRegionales($("#txtUsuarios_Crear_Regional"));
	CargarRegionales($("#txtUsuarios_Regional"));

		CargarRegionales($("#cboActo_Regional"));
//		$("#cboActo_Regional").combobox();
	CargarDepartamentos($("#cboActo_Departamento"));
//		$( "#cboActo_Departamento" ).combobox();
	CargarProyectos($("#cboActo_Proyecto"));
//		$("#cboActo_Proyecto").combobox();
	CargarAreas($("#cboActo_Area"), 0);
//		$("#cboActo_Area").combobox();

		

	cargarReporte1();

	cargarRoles();

	DescargarMunicipios();
//		$( "#cboReporte_Municipio" ).combobox();

}
function AgregarPublicacion(pUsuario, Texto, Fecha)
{
	var tds = "<div><div class='Dashboard_artPublicacion'>";
			tds += "<h5>" + pUsuario + ": <span>" + Texto + "</span></h5>";
			tds += "<h4>" + Fecha +  "</h4></div>";
			tds += "<form class='Dashboard_frmComentar'><input class='txtDashboard_ComentarPublicacion' type='text'/><button class='btnDashboard_ComentarPublicacion'>Agregar Comentario</button></form></div>";
	
		var Elementos = tds + $("#Dashboard_Publicaciones").html();
		$("#Dashboard_Publicaciones div").remove();

		$("#Dashboard_Publicaciones").append(Elementos);
}
function artReporte_Actos_Item_Click()
{
	var idItem = $(this).attr("id").replace("artReporte_Actos_Item-", "");
	$("#Reporte_SubItems article").remove();
	Reporte1_Item = $(this).attr("id").replace("artReporte_Actos_Item-", "");

	$.each(Reporte1_SubItems, function(index, value)
		{
			if (value.Item == idItem)
			{
				var tds = "<article id='artReporte_Actos_SubItem-" + value.Id + "' class='artReporte_Actos_SubItem'>";
						tds += "<strong>"+ value.Codigo+ " </strong>"
		  				tds += "<label>" + value.Nombre + "</h5></label></article>";
		  		$("#Reporte_SubItems").append(tds);
			}
		});
	$(".Reporte1_ItemSeleccionado").removeClass("Reporte1_ItemSeleccionado");
	$(this).addClass("Reporte1_ItemSeleccionado");

}
function artReporte_Actos_SubItem_Click()
{
	$(".Reporte1_SubItemSeleccionado").removeClass("Reporte1_SubItemSeleccionado");
	$(this).addClass("Reporte1_SubItemSeleccionado");	
	Reporte1_SubItem = $(this).attr("id").replace("artReporte_Actos_SubItem-", "");
}
function btnReporte_CrearProyecto_Click(argumento)
{
	argumento.preventDefault();
	if ($("#txtReporte_NuevoProyecto").val() != "")
	{
		$.post("php/CrearProyecto.php", { Nombre: $("#txtReporte_NuevoProyecto").val()}, function(data)
			{
				data = parseInt(data);
				if (data > 0)
				{
					var tds = "<option value='" + data + "'>" + $("#txtReporte_NuevoProyecto").val() + "</option>";
					$("#cboReporte_Proyecto").append(tds);
					$("#cboReporte_Proyecto").val(data);
					btnReporte_CrearProyecto_Cancelar_Click(argumento);		
				}
				
			});
	}
}
function btnReporte_CrearProyecto_Cancelar_Click(argumento)
{
	argumento.preventDefault();
	$("#Reporte_CrearProyecto").slideUp();
	$("#txtReporte_NuevoProyecto").val("");
}
function btnReporte_Guardar_Click()
{
	var obj = $("#frmRep1 label");
	
	var ContadorDeCampos = 0;
	var CamposVacios = "";
	$.each(obj, function(index, value)
		{

			var idInput = $(value).attr("for");
			if ($("#" + idInput).val() == "" || $("#" + idInput).val() == 0)
			{
				ContadorDeCampos++;
				CamposVacios += $(value).text() + ", ";
			}
		});
	if (ContadorDeCampos > 0)
	{
		if (ContadorDeCampos == 1)
		{
			MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el campo " + CamposVacios);
		} else
		{
			MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar los campos " + CamposVacios);
		}
	}
}
function btnUsuarios_Crear_Click(argumento)
{
	argumento.preventDefault();
	$("#Usuarios_Crear").dialog({
					autoOpen: false, 				
					minWidth: 450,
					modal: true,
					title: "Crear Usuario",
					buttons: [
								{
									text: "Guardar",
									click: function() { 

															if ($("#txtUsuarios_Crear_Usuario").val != "")
															{
																if ($("#txtUsuarios_Crear_Correo").val() != "" )
																{
																	if ($("#txtUsuarios_Crear_Nombre").val() != "")
																	{
																		$.post("php/CrearUsuario.php", 
																		{
																			Usuario: $("#txtUsuarios_Crear_Usuario").val(),
																			Correo: $("#txtUsuarios_Crear_Correo").val(),
																			Rol : $("#txtUsuarios_Crear_Rol").val(),
																			Nombre: $("#txtUsuarios_Crear_Nombre").val(),
																			Regional: $("#txtUsuarios_Crear_Regional").val(),
																			Area: $("#txtUsuarios_Crear_Area").val()
																		}, function(data)
																				{
																					if (parseInt(data) > 0)
																					{
																						MostrarAlerta("default", "ui-icon-check", "hey!", "Usuario Creado, por favor revise su correo");					
																						$("#txtUsuarios_Crear_Usuario, #txtUsuarios_Crear_Nombre, #txtUsuarios_Crear_Correo").val("");
																					}else
																					{
																						MostrarAlerta("error", "ui-icon-alert", "Error!", data);
																					}
																				});
																	} else
																	{
																		MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Nombre Completo");				
																	}
																} else
																{
																	MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Correo");		
																}
															} else
															{
																MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Usuario");
															}
													  }
								},
								{
									text: "Cancelar",
									click: function() { 
														$("#Usuarios_Crear").dialog('close');
													  }
								}
							  ]
									});
			$("#Usuarios_Crear").dialog('open');


}
function btnUsuarios_Editar_Click(argumento)
{
	argumento.preventDefault();
	var obj = $(this).parent("td").parent("tr").find("td");
	
	$("#txtUsuarios_Nombre").val($(obj[2]).text());
	$.each($("#txtUsuarios_Estado option"), function(index, value)
			{
				if ($(value).text() == $(obj[3]).text())
				{
					$("#txtUsuarios_Estado").val($(value).val());
				}
			});

	$.each($("#txtUsuarios_Rol option"), function(index, value)
			{
				if ($(value).text() == $(obj[5]).text())
				{
					$("#txtUsuarios_Rol").val($(value).val());
				}
			});

	$.each($("#txtUsuarios_Regional option"), function(index, value)
			{
				if ($(value).text() == $(obj[6]).text())
				{
					$("#txtUsuarios_Regional").val($(value).val());
				}
			});

	$.each($("#txtUsuarios_Area option"), function(index, value)
			{
				if ($(value).text() == $(obj[7]).text())
				{
					$("#txtUsuarios_Area").val($(value).val());
				}
			});

	$("#btnEditarUsuario_CambiarClave").attr("consecutivo", $(obj[0]).text());

	$("#Usuarios_Editar").dialog({
					autoOpen: false, 				
					minWidth: 450,
					modal: true,
					title: "Editar " + $(obj[1]).text() + ": " + $(obj[4]).text(),
					buttons: [
								{
									text: "Guardar",
									click: function() { 
															$.post("php/ActualizarUsuario.php", 
																{
																	Consecutivo : $(obj[0]).text(),
																	Nombre : $("#txtUsuarios_Nombre").val(),
																	Estado : $("#txtUsuarios_Estado").val(),
																	Rol : $("#txtUsuarios_Rol").val(),
																	Regional : $("#txtUsuarios_Regional").val(),
																	Area : $("#txtUsuarios_Area").val()
																}, 
																function(data)
																{
																	if (data == 1)
																	{
																		$("#Usuarios_Editar").dialog('close');
																		//CargarUsuarios();	
																	}
																	
																});
															
													  }
								},
								{
									text: "Cancelar",
									click: function() { 
														$("#Usuarios_Editar").dialog('close');
													  }
								}
							  ]
									});
			$("#Usuarios_Editar").dialog('open');

}
function CambiarEstadoReporte(Consecutivo, EstadoNuevo, Fila)
{
	$("#txtRepDiligencioados_Observaciones").val("");
	if (EstadoNuevo ==4)
	{
		$("#txtReporte_FechaCierre").hide();
		$("#lblReporte_FechaCierre").hide();	
	} else
	{
		$("#txtReporte_FechaCierre").show();
		$("#lblReporte_FechaCierre").show();	
	}
	

				   		
	$("#RepDiligencioados_Observaciones").dialog({
					autoOpen: false, 				
					minWidth: 450,
					modal: true,
					title: "Observaciones de cambio de estado",
					buttons: [
								{
									text: "Aceptar",
									click: function() { 
															$.post("php/CambiarEstadoReporte.php", 
																{
																	Consecutivo : Consecutivo,
																	Usuario : Usuario.Id,
																	Observaciones : $("#txtRepDiligencioados_Observaciones").val(),
																	Estado : EstadoNuevo,
																	Fecha: obtenerFecha() + " " + obtenerHora(),
																	idAreaResponsable : "0"
																}, 
																function(data)
																{
																	if (data != 0)
																	{
																		$(Fila).text(data);
																		$("#RepDiligencioados_Observaciones").dialog('close');
																	}
																	
																});
															
													  }
								},
								{
									text: "Cancelar",
									click: function() { 
														$("#RepDiligencioados_Observaciones").dialog('close');
													  }
								}
							  ]
									});
			$("#RepDiligencioados_Observaciones").dialog('open');
}
function CargarAreas(obj, pSac)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarAreas.php", {SAC: pSac}, function(data)
		{
			var tds = "";
			$.each(data, function(index, value)
				{
					tds = "<option value='" + value.idArea + "'>" + value.Nombre + "</option>";
					$(obj).append(tds);
				});
		}
		, "json");
}
function CargarCausaRaiz()
{
	$.post("php/CargarCausaRaiz.php",{}, function(data)
		{
			$("#Reporte_CausaRaiz article").remove();
			var tds = "";
			$.each(data, function(index, value)
				{
					$("#Reporte_CausaRaiz").append("<article Consecutivo='" + value.Consecutivo + "' class='itemCausaRaiz'>" + value.Nombre + "</article>");
				});

		}, 'json');
}

function CargarDepartamentos(obj)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarDepartamentos.php", {}, function(data)
		{
			var tds = "";
			$.each(data, function(index, value)
				{
					tds = "<option value='" + value.idDepartamento + "'>" + value.Nombre + "</option>";
					$(obj).append(tds);
				});
		}
		, "json");
}

function cargarMunicipios(idDepartamento, obj)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	var tds = "";
	$.each(Municipios, function(index, value)
	{
		if (value.idDepartamento == idDepartamento)
		{
			tds = "<option value='" + value.idMunicipio + "'>" + value.Nombre + "</option>";
			$(obj).append(tds);
		}
	});
}
function CargarPermisos()
{
	if (Usuario.Rol >  1)
	{
		$("#MainMenu li").hide('slide');
		$("#lnkLogout").show('slide');
	}
	if (Usuario.Rol == 2)
	{
		$("#lnkDashboard").show('slide');
	}
}
function CargarPregunta()
{
	if (DisponibilidadPreguntas > 0)
	{		
		$.post("php/CargarTest.php", {Usuario: Usuario.Id}, function(data)
			{
				var tds = "<article id='radio'><p>" + data.Pregunta + "</p>";
				
				$.each(data.Respuestas, function(index, value)
					{
						tds += "<input class='radio_Preguntas' type='radio' id='rTestR_" + value.Id + "' name='rTest_"+ data.Id + "' valor='" + value.Valor + "'/>";
						tds += "<label class='radio_Preguntas' for='rTestR_" + value.Id + "'>" + value.Texto + "</label>";
					});
				tds += "</article>";

				$("#SeguirTest_Pregunta article").remove();
				$("#SeguirTest_Pregunta").append(tds);
				$("#radio").buttonset();

				$("#SeguirTest_Pregunta").dialog({
					autoOpen: false, 				
					closeOnEscape: false,
					minWidth: 600,
					modal: true,
					title: "Pregunta " + data.Id,
					buttons: [
								{
									text: "Guardar",
									click: function() { 

															$.post("php/GuardarPregunta.php",
															{
																Usuario: Usuario.Id,
																Pregunta: data.Id,
																Respuesta: $("#SeguirTest_Pregunta input[name=rTest_" + data.Id + "]:checked'").attr("valor"),
																idRespuesta: $("#SeguirTest_Pregunta input[name=rTest_" + data.Id + "]:checked'").attr("id").replace("rTestR_", ""),
																Fecha: obtenerFecha() + " " + obtenerHora()
															}, function(dataP)
															{
																if (parseInt(dataP) == 1)
																{
																	$( "#SeguirTest_Pregunta" ).dialog( "close" );
																	MostrarAlerta("default", "ui-icon-circle-check", "Hey!", "Pregunta Guardada"); 		
																	DisponibilidadPreguntas = DisponibilidadPreguntas - 1;
																}
																
															});
													  }
								}
							  ]
									});
			$("#SeguirTest_Pregunta").dialog('open');
			var widget = $( "#SeguirTest_Pregunta" ).dialog( "widget" );
			$(widget).find("a.ui-dialog-titlebar-close").hide();
			}, "json");
	}
	else
	{
		MostrarAlerta("error", "ui-icon-alert", "Hey!", "No tiene suficientes puntos para contestar las preguntas, recuerde diligenciar sus reportes.");
	}
}
function CargarRegionales(obj)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarRegionales.php", {}, function(data)
		{
			var tds = "";
			$.each(data, function(index, value)
				{
					tds = "<option value='" + value.idRegional + "'>" + value.Nombre + "</option>";
					$(obj).append(tds);
				});
		}
		, "json");
}
function CargarProyectos(obj)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarProyectos.php", {}, function(data)
		{
			var tds = "<option value='0'>Seleccione uno</option>";
			$.each(data, function(index, value)
				{
					tds += "<option value='" + value.idProyecto + "'>" + value.Nombre + "</option>";
				});
					$(obj).append(tds);
		}
		, "json");
}
function cargarReporte1()
{
	$.post("php/CargarItems_Reporte1.php", {}, function(data)
		{
			Reporte1_Items = data;
			$.each(data, function(index, value)
			{
				var tds = "<article id='artReporte_Actos_Item-" + value.Id + "' class='artReporte_Actos_Item'>";
						tds += "<strong>"+ value.Id + "</strong>"
		  				tds += "<h5>" + value.Nombre + "</h5></article>";
		  		$("#Reporte_Items").append(tds);
			});
		}
		, "json");

	$.post("php/CargarSubItems_Reporte1.php", {}, function(data){Reporte1_SubItems = data;}, "json");
}
function cargarRoles()
{
	$.post("php/CargarRoles.php", {}, function(data)
		{
			$.each(data, function(index, value)
			{
				var tds = "<option value='" + value.idRol + "'>" + value.Nombre + "</option>";
		  		$("#txtUsuarios_Rol").append(tds);
		  		$("#txtUsuarios_Crear_Rol").append(tds);
			});
		}
		, "json");

	$.post("php/CargarSubItems_Reporte1.php", {}, function(data){Reporte1_SubItems = data;}, "json");
}
function CargarUsuarios(evento)
{
	evento.preventDefault();
	if ($("#txtUsuarios_Buscar").val() != "")
	{
		DestruirTabla($("#tableUsuarios"));
		$.post("php/CargarUsuarios.php", {parametro: $("#txtUsuarios_Buscar").val()}, function(data)
			{
				var tds = "";
				var tableBody = $("#tableUsuarios").find("tbody");
					$.each(data, function (index, value) 
					{
						tds = "<tr>";
						tds += "<td>" + value.Consecutivo + "</td>";
						tds += "<td>" + value.Usuario + "</td>";
						tds += "<td>" + value.Nombre + "</td>";
						tds += "<td>" + value.Estado + "</td>";
						tds += "<td>" + value.Correo + "</td>";
						tds += "<td>" + value.Rol + "</td>";
						tds += "<td>" + value.Regional + "</td>";
						tds += "<td>" + value.Area + "</td>";
						tds += "<td><button Consecutivo='" + value.Consecutivo + "' class='btnUsuarios_Editar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary' title='Editar " + value.Usuario + "'><span class='ui-button-icon-primary ui-icon ui-icon-person'></span></button></td>";
						tds += "</tr>";

						tableBody.append(tds);
					});

				crearTabla($("#tableUsuarios"), "CTW");
			}, "json");
	}
}
function crearTabla(obj, parametros)
{
	$(obj).dataTable( {
					"sDom": parametros + '<"clear">lfrtip',
					"aaSorting": [[ 0, "asc" ]],
					"oTableTools": 
						{
					"sSwfPath": "Tools/datatable/media/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
		                "print",
		                {
		                    "sExtends":    "collection",
		                    "sButtonText": "Guardar",
		                    "aButtons":    [ "csv", "xls", "pdf" ]
		                }
		            			]
						},
						"oColumnFilterWidgets": 
						{
							"sSeparator": "\\s*/+\\s*",
							"bGroupTerms" : true
						}
				} );
	var idTabla = $(obj).attr("id");
	var objFiltros = $("#" + idTabla + "_wrapper .column-filter-widget");
	

	$(objFiltros[2]).hide();
}
function Dashboard_frmComentar_Submit(argumento)
{
	argumento.preventDefault();
	var txtObj = $(this).find("input");
	if ($(txtObj).val() != "")
	{
		var obj = $(this).parent("div").find(".Dashboard_artPublicacion");

		var tds = "<div><div class='Dashboard_artComentario'>";
				tds += "<h5>" + Usuario.Nombre + ": <span>" + $(txtObj).val() + "</span></h5>";
				tds += "<h4>" + obtenerFecha() + " " + obtenerHora() +  "</h4></div>";
		$(obj).append(tds);
		$(txtObj).val("");
	}
}
function Dashboard_Publicar_Submit(argumento)
{
	argumento.preventDefault();
	if ($("#txtDashboard_Publicacion").val() != "")
	{
		AgregarPublicacion(Usuario.Nombre, $("#txtDashboard_Publicacion").val(), obtenerFecha() + " " + obtenerHora())
		$("#txtDashboard_Publicacion").val("");	
	}
}
function DescargarMunicipios()
{
	$.post("php/CargarMunicipios.php", {}, function(data)
		{
			Municipios = data;
		}
		, "json");
}
function DestruirTabla(obj)
{
	$(obj).dataTable().fnDestroy();
	$(obj).find("tbody").find("tr").remove();
}

function frmRep1_Submit(argumento)
{
	argumento.preventDefault();
	
	if (Reporte1_SubItem > 0)
	{
		if ($("#txtReporte_Correctivos").val() != "" && $("#cboReporte_Responsable").val() > 0)
		{
			if ($("#txtReporte_Descripcion").val() != "")
			{
				if ($("#cboReporte_Regional").val() > 0 && $("#cboReporte_Area").val() > 0 && $("#cboReporte_Departamento").val() > 0 && $("#cboReporte_Municipio").val() > 0 && $("#cboReporte_Proyecto").val() > 1)
				{
					if (Reporte_CausaRaiz > 0)
					{
						$.post("php/CrearReporte1.php", 
							{
								Nombre: $("#txtReporte_Nombre").val(),
								Fecha: $("#txtReporte_Fecha").val(),
								Cargo: $("#txtReporte_Cargo").val(),
								Descripcion: $("#txtReporte_Descripcion").val(),
								Correctivos: $("#txtReporte_Correctivos").val(),
								Calificacion: Reporte1_Tarjeta,
								Item: Reporte1_Item,
								SubItem: Reporte1_SubItem,
								Usuario: Usuario.Id,
								idMunicipio: $("#cboReporte_Municipio").val(),
								idDepartamento: $("#cboReporte_Departamento").val(),
								idProyecto: $("#cboReporte_Proyecto").val(),
								idArea: $("#cboReporte_Area").val(),
								idAreaResponsable: $("#cboReporte_Responsable").val(),
								idRegional : $("#cboReporte_Regional").val(),
								idCausa : Reporte_CausaRaiz
							}, function(data)
							{
								if (data == 1)
								{
									MostrarAlerta("default", "ui-icon-circle-check", "Hey!", "El reporte ha sido creado");
									$("#frmRep1 input").val("");
									$("#frmRep1 textarea").val("");
									$(".Reporte1_ItemSeleccionado").removeClass("Reporte1_ItemSeleccionado");
									$(".Reporte1_SubItemSeleccionado").removeClass("Reporte1_SubItemSeleccionado");
									$(".itemCausaRaizSeleccionado").removeClass("itemCausaRaizSeleccionado");
									$("#Reporte_SubItems article").remove();

									Reporte1_Item = 0;
									Reporte1_SubItem = 0;
									Reporte1_Tarjeta = 'Ninguna';

									
									Reporte_CausaRaiz = 0;

									DisponibilidadPreguntas = DisponibilidadPreguntas  + 3;
								} else
								{
									MostrarAlerta("error", "ui-icon-alert", "Error!", "El reporte no ha sido creado, por favor vuelva a intentarlo");
								}
							});
					}else
					{
						MostrarAlerta("error", "ui-icon-alert", "Error!", "Es importante diligenciar la Causa Raiz");				
					}
				} else
				{
					MostrarAlerta("error", "ui-icon-alert", "Error!", "Faltan datos por llenar en la Pestaña de Identificación");		
				}
			} else
			{
				MostrarAlerta("error", "ui-icon-alert", "Error!", "Faltan datos por llenar en la Pestaña de Descripcion");
			}
		} else
		{
			MostrarAlerta("error", "ui-icon-alert", "Error!", "Faltan datos por llenar en la Pestaña de Correctivos");
		}
	} else
	{
		MostrarAlerta("error", "ui-icon-alert", "Error!", "Debe escoger los Actos y Condiciones");
	}
}
function itemCausaRaiz_Click()
{
	Reporte_CausaRaiz = $(this).attr("Consecutivo");
	$(".itemCausaRaizSeleccionado").removeClass("itemCausaRaizSeleccionado");
	$(this).addClass("itemCausaRaizSeleccionado");

}
function RepDiligenciados_Buscar_Click (argumento) 
{
	argumento.preventDefault();
	DestruirTabla($("#RepDiligenciados_Table"));
	var tds = "";
	$(".imgCargando").show();
	$.post("php/CargarReporte1.php", 
		{
			Desde: $("#txtRepDiligenciados_Desde").val(),
			Hasta: $("#txtRepDiligenciados_Hasta").val(),
			Regional: Usuario.Regional,
			Area : Usuario.Area
		}, function(data)
		{
			if (data != 0)
			{
				var tableBody = $("#RepDiligenciados_Table").find("tbody");
				$.each(data, function (index, value) 
				{
					tds = "<tr Consecutivo='" + value.Consecutivo + "'>";
					tds += "<td>" + value.Consecutivo + "</td>";
					tds += "<td>" + value.Fecha + "</td>";
					tds += "<td>" + value.Nombre + "</td>";
					tds += "<td>" + value.Cargo + "</td>";
					tds += "<td>" + value.Regional + "</td>";
					tds += "<td>" + value.Area + "</td>";
					tds += "<td>" + value.Departamento + "</td>";
					tds += "<td>" + value.Municipio + "</td>";
					tds += "<td>" + value.Proyecto + "</td>";
					tds += "<td>" + value.Item + "</td>";
					tds += "<td>" + value.SubItem + "</td>";
					tds += "<td>" + value.AreaResponsable + "</td>";
					tds += "<td>" + value.CausaRaiz + "</td>";
					tds += "<td>" + value.Tarjeta + "</td>";
					tds += "<td>" + value.Usuario + "</td>";
					tds += "<td>" + value.Estado + "</td>";
					tds += "</tr>";

					tableBody.append(tds);
				});
			}

			$(".imgCargando").hide();
			crearTabla($("#RepDiligenciados_Table"), "CTW");
		}
		, "json");

}
function RepDiligenciados_Table_tr_Click()
{
	var Consecutivo = $(this).attr("Consecutivo");
	$("#btnRepDiligenciados_Estados").attr("idReporte", Consecutivo);
	$("#btnRepDiligenciados_CambiarArea").attr("idReporte", Consecutivo);
	var objFila = $(this).find("td");
	
	$.post("php/CargarReporte1_Detalles.php", {Consecutivo: Consecutivo}, function(data) 
	{
		var ColorTarjeta = "#FFFFFF";
		if (data != 0)
		{
			if (data.Tarjeta == "Ninguna")
			{
				ColorTarjeta = "#F2F2F2";
			}
			if (data.Tarjeta == "Amarilla")
			{
				ColorTarjeta = "yellow";
			}
			if (data.Tarjeta == "Roja")
			{
				ColorTarjeta = "red";
			}

			$("#RepDiligenciados_Calificacion div").css("background", ColorTarjeta);
			$("#RepDiligenciados_Descripcion").text(data.Descripcion);
			$("#RepDiligenciados_Correctivos").text(data.Correctivos);

			$("#RepDiligenciados_Detalle").dialog(
				{
					autoOpen: false,
					minWidth: 720,
					title: "Reportado por: " + $(objFila[14]).text() + " de: " + $(objFila[5]).text() + " el día " + $(objFila[1]).text(),
					buttons: [
								{
									text: "Gestión",
									click: function() { 
															CambiarEstadoReporte($(objFila[0]).text(), 1, $(objFila[15]));
													  }
								},
								{
									text: "No Acepto",
									click: function() { 
															CambiarEstadoReporte($(objFila[0]).text(), 2, $(objFila[15]));	
													  }
								},
								{
									text: "Corregido",
									click: function() { 
															CambiarEstadoReporte($(objFila[0]).text(), 3, $(objFila[15]));	
													  }
								}
							  ]
				});	
			$("#RepDiligenciados_Detalle").dialog("open");
			$("#RepDiligenciados_Estados").hide();
			$("#RepDiligenciados_Detalles").show();
		}
	}, "json");
}
function Reporte_Calificacion_article_click (argumento) 
{
	argumento.preventDefault();
	$(".TarjetaSeleccionada").removeClass("TarjetaSeleccionada");
	$(this).addClass("TarjetaSeleccionada");
	Reporte1_Tarjeta = $(this).attr("valor");
}
function txtUsuarios_Crear_Correo_Change()
{
	var tmpCorreo = $("#txtUsuarios_Crear_Correo").val().split("@");
	if (typeof(tmpCorreo[1]) != "undefined")
	{
		if (tmpCorreo[1] != "wspgroup.com")
		{
			$("#txtValidarCorreo").slideDown();
			$("#txtUsuarios_Crear_Correo").val("");
		} else
		{
			$("#txtValidarCorreo").slideUp();
		}
	} 
}
function txtUsuarios_Crear_Usuario_Change()
{
	$("#txtUsuarios_Crear_Usuario").val($("#txtUsuarios_Crear_Usuario").val().replace(/ /gi, ""));
	$("#txtUsuarios_Crear_Usuario").val($("#txtUsuarios_Crear_Usuario").val().replace(/ñ/gi, "n"));
	$("#txtUsuarios_Crear_Usuario").val($("#txtUsuarios_Crear_Usuario").val().toLowerCase());
}
function btnRepDiligenciados_Detalles_Click()
{
	$("#RepDiligenciados_Estados").show("slide");
	$("#RepDiligenciados_Detalles").hide();	

	var idReporte = $("#btnRepDiligenciados_Estados").attr("idReporte");

	$.post("php/CargarReporte1_Estados.php", {Consecutivo : idReporte}, function(data)
		{
			var tds = "";
			var tableBody = $("#tableRepDiligenciados_Detalles").find("tbody");
			DestruirTabla($("#tableRepDiligenciados_Detalles"));
			$(tableBody).find("tr").remove();


				$.each(data, function (index, value) 
				{
					tds = "<tr>";
					tds += "<td>" + value.Fecha + "</td>";
					tds += "<td>" + value.Anterior + "</td>";
					tds += "<td>" + value.Nuevo + "</td>";
					tds += "<td>" + value.Usuario + "</td>";
					tds += "<td>" + value.Observaciones + "</td>";
					tds += "</tr>";

					tableBody.append(tds);
				});

			crearTabla($("#tableRepDiligenciados_Detalles"), "CTW");
		}, "json");
}
function graficas_CargarParametros()
{
	var objLi = $("#Graficas_ejeX li");
  	var tds = "";
  	$.each(objLi, function(index, value)
	{
		if (typeof $(value).attr("par") != "undefined")
		{	
			if (index == 0)
			{
				tds = $(value).attr("par");
			} else
			{
				tds += ', ' + $(value).attr("par");
			}
		}
	});
	$("#txtGraficas_Parametros").val(tds);
}
function cargarConveciones(datos2)
{
	var tmpObj = $(".flotr-legend-color-box");
				$("#divGraficasConvenciones article").remove();
				$.each(tmpObj, function(index, value)
				{
					var objHtml = $(value).html();
									
					var strObj = objHtml.substring(parseInt(objHtml.indexOf("(") + 1 ), parseInt(objHtml.indexOf(")")) );
					var strObj_ = strObj.split(",");
					
					$("#divGraficasConvenciones").append("<article><div style='width:1em; height:1em;background:#"+ rgbToHex(strObj_[0], strObj_[1], strObj_[2]) + ";'></div><span> " + datos2[index].label+"</span></article>");

				});
				$(".flotr-legend").hide();
}
function cargarGrafica(datos2, titulo)
{
	var Contenedor = document.getElementById("divGraficas");
	graph = Flotr.draw(Contenedor,
					datos2
					, {
						title: titulo,
					    bars : {
					      show : true,
					      horizontal : false,
					      shadowSize : 5,
					      barWidth: 1
					    },
					    legend: 
					    {
					    	backgroundOpacity: 0,
					    	position: 'ne'
					    },
					    grid : 
					    {
					      verticalLines : false,
					      horizontalLines : true
					    },
					    mouse:
					    {
					    	relative: true,
					    	track: true,
					    	tackAll: true,
					    	trackDecimals: 0,
					    	trackFormatter: function (x) 
					    	{
					        	return x.series.label +": " + x.series.data[0][1];
					      	}
					    },
					     xaxis: 
					    {  	autoscale: true,
					    	showLabels: false   },
					    yaxis:
					    {  	
					    	autoscale:true,
					    	autoscaleMargin : 1,
					    	min: 0,
					    	showLabels: true,
					    	tickDecimals: 0
					    }
						});
}
function verGrafica()
{
	var Contenedor = document.getElementById("divGraficas");

	var d1 = [];
	var datos2 = new Array;

	$.post("php/GraficarReporte1.php",
		{
			Desde: $("#txtGraficas_Desde").val(),
			Hasta: $("#txtGraficas_Hasta").val(),
			Regional: Usuario.Regional,
			Area : Usuario.Area,
			Parametros : $("#txtGraficas_Parametros").val()
		},
		function(datos)
		{
			$.each(datos, function(index, value)
					{
						d1.push([index, parseInt(value.Cantidad)]);
						datos2[index] = {"data": [d1[index]], "label": value.label};
					}
				);
			var pTitulo = $("#txtGraficas_Parametros").val().replace(/,/g, " y");
			cargarGrafica(datos2, pTitulo);
			cargarConveciones(datos2);

		}
	,"json");
}
function rgbToHex(R,G,B) 
{
	return toHex(R)+toHex(G)+toHex(B);
}
function toHex(n) 
{
	 n = parseInt(n,10);
	 if (isNaN(n)) return "00";
	 n = Math.max(0,Math.min(n,255));
	 return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
}
function btnEditarUsuario_CambiarClave_Click(evento)
{
	evento.preventDefault();
	$("#txtEditarUsuario_Clave, #txtEditarUsuario_NClave").val("");
	$("#Clave_Editar").dialog({
					autoOpen: false, 				
					minWidth: 450,
					modal: true,
					title: "Cambiar Clave",
					buttons: [
								{
									text: "Guardar",
									click: function() { 

															if ($("#txtEditarUsuario_Clave").val != "")
															{
																if ($("#txtEditarUsuario_Clave").val() == $("#txtEditarUsuario_NClave").val() )
																{
																	$.post("php/editarClave.php", 
																	{
																		Usuario: $("#btnEditarUsuario_CambiarClave").attr("consecutivo"),
																		Clave: $("#txtEditarUsuario_Clave").val()
																	}, function(data)
																			{
																				if (parseInt(data) > 0)
																				{
																					MostrarAlerta("default", "ui-icon-check", "hey!", "La clave ha sido cambiada");					
																					$("#txtEditarUsuario_Clave, #txtEditarUsuario_NClave").val("");
																					$("#Clave_Editar").dialog('close');
																				}else
																				{
																					MostrarAlerta("error", "ui-icon-alert", "Error!", data);
																				}
																			});
																} else
																{
																	MostrarAlerta("error", "ui-icon-alert", "Error!", "Las Claves deben coincidir");		
																}
															} else
															{
																MostrarAlerta("error", "ui-icon-alert", "Error!", "Diligencia alguna Clave");		
															}
													  }
								},
								{
									text: "Cancelar",
									click: function() { 
														$("#Clave_Editar").dialog('close');
													  }
								}
							  ]
									});
			$("#Clave_Editar").dialog('open');
}
function btnActo_CrearProyecto_Click(argumento)
{
	argumento.preventDefault();
	if ($("#txtActo_NuevoProyecto").val() != "")
	{
		$.post("php/CrearProyecto.php", { Nombre: $("#txtActo_NuevoProyecto").val()}, function(data)
			{
				data = parseInt(data);
				if (data > 0)
				{
					var tds = "<option value='" + data + "'>" + $("#txtActo_NuevoProyecto").val() + "</option>";
					$("#cboActo_Proyecto").append(tds);
					$("#cboActo_Proyecto").val(data);
					btnActo_CrearProyecto_Cancelar_Click(argumento);		
				}
				
			});
	}
}
function btnActo_CrearProyecto_Cancelar_Click(argumento)
{
	argumento.preventDefault();
	$("#Acto_CrearProyecto").slideUp();
	$("#txtActo_NuevoProyecto").val("");
}
function frmRep2_Submit(evento)
{
	evento.preventDefault();
	var obj = $("#Acto_Identificacion input");
	var validador = 0;
	var Estados = "";
	var Errores = "";
	var Consecuencia = "";
	var Prevencion = "";

	$.each(obj, function(index, val) 
	{
		 if ($(val).val() == "")
		 {
		 	if ($(val).attr("id") != "txtActo_NuevoProyecto")
		 	{
		 		validador++;
		 	}
		 }
	});
	obj = $("#Acto_Identificacion select");
	$.each(obj, function(index, val) 
	{
		 if ($(val).val() == 0)
		 {
		 	alert($(val).attr("id"));
		 	validador++;
		 }
	});
	if (validador == 0)
	{
		if ($("#txtActo_Descripcion").val() != "")
		{
			obj = $(".btnActos_Estado_");
			$.each(obj, function(index, val) 
			{
				 if ($(val).attr("valor") == 1)
				 {
				 	Estados += ", " + $(val).find("h5").text();
				 }
			});
			 if (Estados != "")
			 {
			 	Estados = Estados.substr(2);
			 	obj = $(".btnActos_Acto");
				$.each(obj, function(index, val) 
				{
					 if ($(val).attr("valor") == 1)
					 {
					 	Errores += ", " + $(val).find("h5").text();
					 }
				});
				 if (Errores != "")
				 {
				 	Errores = Errores.substr(2);
				 	obj = $(".btnActos_Consecuencia");
					$.each(obj, function(index, val) 
					{
						 if ($(val).attr("valor") == 1)
						 {
						 	Consecuencia += ", " + $(val).find("h5").text();
						 }
					});
					 if (Consecuencia != "")
					 {
					 	Consecuencia = Consecuencia.substr(2);
					 	obj = $(".btnActos_Prevencion");
						$.each(obj, function(index, val) 
						{
							 if ($(val).attr("valor") == 1)
							 {
							 	Prevencion += ", " + $(val).find("h5").text();
							 }
						});
						 if (Prevencion != "")
						 {
						 	Prevencion = Prevencion.substr(2);
						 	$.post("php/CrearActo.php", 
							{
								Nombre: $("#txtActo_Nombre").val(),
								Cedula: $("#txtActo_Cedula").val(),
				                Fecha: $("#txtActo_Fecha").val(),
				                Cargo: $("#txtActo_Cargo").val(),
				                idRegional : $("#cboActo_Regional").val(),
				                idArea: $("#cboActo_Area").val(),
				                idDepartamento: $("#cboActo_Departamento").val(),
				                idMunicipio: $("#cboActo_Municipio").val(),
				                idProyecto: $("#cboActo_Proyecto").val(),
				                Descripcion: $("#txtActo_Descripcion").val(),
				                Estado: Estados,
				                Error: Errores,
				                Consecuencia:Consecuencia,
				                TecPrev:Prevencion,
				                Usuario: Usuario.Id
							}, function(data)
							{
								if (data == 1)
								{
									MostrarAlerta("default", "ui-icon-circle-check", "Hey!", "El Acto ha sido creado");
									$("#frmRep2 input").val("");
									$("#txtActo_Descripcion").val("");
									$("#frmRep2 select").val(0);
									$(".btnActos_Estado").css("background", "none");
									$(".btnActos_Estado").css("color", "black");
									$(".btnActos_Estado").attr("Valor", 0);
								} else
								{
									MostrarAlerta("error", "ui-icon-alert", "Error!", "El reporte no ha sido creado, por favor vuelva a intentarlo");
								}
							});
						 } else
						 {
							MostrarAlerta("error", "ui-icon-alert", "Error!", "Debe seleccionar por lo menos una Técnica de Prevención");		 	
						 }
					 } else
					 {
						MostrarAlerta("error", "ui-icon-alert", "Error!", "Debe seleccionar por lo menos una Consecuencia");		 	
					 }
				 } else
				 {
					MostrarAlerta("error", "ui-icon-alert", "Error!", "Debe seleccionar por lo menos un Error");		 	
				 }
			 } else
			 {
				MostrarAlerta("error", "ui-icon-alert", "Error!", "Debe seleccionar por lo menos un Estado");		 	
			 }
		} else
		{
			MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar campos en la Ficha de Descripción");
		}
	} else
	{
		MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar campos en la Ficha de Identificación");
	}
}
function ActDiligenciados_Buscar_Click(argumento) 
{
	argumento.preventDefault();
	DestruirTabla($("#ActDiligenciados_Table"));
	var tds = "";
	$(".imgCargando").show();
	$.post("php/CargarReporte2.php", 
		{
			Desde: $("#txtActDiligenciados_Desde").val(),
			Hasta: $("#txtActDiligenciados_Hasta").val(),
			Regional: Usuario.Regional,
			Area : Usuario.Area
		}, function(data)
		{
			if (data != 0)
			{
				var tableBody = $("#ActDiligenciados_Table").find("tbody");
				$.each(data, function (index, value) 
				{
					tds = "<tr Consecutivo='" + value.Consecutivo + "'>";
					tds += "<td>" + value.Consecutivo + "</td>";
					tds += "<td>" + value.Fecha + "</td>";
					tds += "<td>" + value.Nombre + "</td>";
					tds += "<td>" + value.Cedula + "</td>";
					tds += "<td>" + value.Cargo + "</td>";
					tds += "<td>" + value.Regional + "</td>";
					tds += "<td>" + value.Area + "</td>";
					tds += "<td>" + value.Departamento + "</td>";
					tds += "<td>" + value.Municipio + "</td>";
					tds += "<td>" + value.Proyecto + "</td>";
					tds += "<td>" + value.Descripcion + "</td>";
					tds += "<td>" + value.Estados + "</td>";
					tds += "<td>" + value.Errores + "</td>";
					tds += "<td>" + value.Consecuencias + "</td>";
					tds += "<td>" + value.Prevenciones + "</td>";
					tds += "<td>" + value.Usuario + "</td>";
					tds += "</tr>";

					tableBody.append(tds);
				});
			}

			$(".imgCargando").hide();
			crearTabla($("#ActDiligenciados_Table"), "CTW");
		}
		, "json");

}
function btnRepDiligenciados_CambiarArea_Click()
{
	var idReporte = $("#btnRepDiligenciados_CambiarArea").attr("idReporte");
	$("#RepDiligenciados_CambiarArea").dialog({
		autoOpen: false, 				
		minWidth: 450,
		modal: true,
		title: "Crear Usuario",
		buttons: [
					{
						text: "Guardar",
						click: function() { 
											if ($("#txtCambiarArea_Area").val() > 0)
											{
												$.post("php/CambiarEstadoReporte.php", 
																	{
																		Consecutivo : idReporte,
																		Usuario : Usuario.Id,
																		Observaciones : $("#txtRepDiligenciados_CambiarArea_Observaciones").val(),
																		Estado : 0,
																		Fecha: obtenerFecha() + " " + obtenerHora(),
																		idAreaResponsable : $("#txtCambiarArea_Area").val()
																	}, function()
																	{
																		$("#txtCambiarArea_Area").val(0);
																		$("#txtRepDiligenciados_CambiarArea_Observaciones").val("");
																	});
												$("#RepDiligenciados_CambiarArea").dialog('close');
											} else
											{
												$("#txtCambiarArea_Area").focus();
											}
										}
					},
					{
						text: "Cancelar",
						click: function() { 
											$("#RepDiligenciados_CambiarArea").dialog('close');
										  }
					}
				  ]
						});
$("#RepDiligenciados_CambiarArea").dialog('open');
}