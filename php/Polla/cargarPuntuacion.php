<?php 
   include("../conectar.php"); 
	$link=Conectarse(); 

	$Index = 0;	
	
	class EquipoClasificado
	{
		public $Regional;
		public $Nombre;
		public $Puntos;
		public $Televisor;
	}

	$sql = "SELECT 
				Regionales.Nombre AS 'Regional',
				DatosUsuarios.Nombre AS 'Nombre', 
				DatosUsuarios.tv AS 'Televisor', 
			    SUM(puntos.cantidad) AS 'Puntos'
			FROM 
				DatosUsuarios,
			    puntos,
			    Regionales
			WHERE
				DatosUsuarios.idLogin = puntos.idLogin
			    AND DatosUsuarios.idRegional = Regionales.idRegional
			GROUP BY
				Regionales.Nombre,
				DatosUsuarios.Nombre,
				DatosUsuarios.tv
			ORDER BY 1, 4 DESC;";
			
	$result=  mysql_query($sql, $link);

	while($row = mysql_fetch_array($result))
	{ 
		$Partidos[$Index] = new EquipoClasificado();
		$Partidos[$Index]->Regional = utf8_encode($row['Regional']);
		$Partidos[$Index]->Nombre = utf8_encode($row['Nombre']);
		$Partidos[$Index]->Puntos = utf8_encode($row['Puntos']);
		$Partidos[$Index]->Televisor = utf8_encode($row['Televisor']);

		$Index++;	
	}

		
	mysql_close($link);	
	echo json_encode($Partidos);
?> 
