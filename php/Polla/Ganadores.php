<?php
	include("../conectar.php"); 
	$link=Conectarse();

	$sql = "INSERT INTO puntos 
			SELECT 
				apuesta1F.idLogin, 
				400,
			    CONCAT('Resultado Exacto Partido: ', equipoLocal.Nombre, ' vs ', equipoVisitante.Nombre)
			FROM 
				apuesta1F, 
			    resultados,
			    partidos,
			    equipos AS equipoLocal,
			    equipos AS equipoVisitante
			WHERE
				apuesta1F.idPartido = resultados.idPartido
			    AND apuesta1F.golesLocal = resultados.golesLocal
			    AND apuesta1F.golesVisitante = resultados.golesVisitante
			    AND partidos.idPartido = apuesta1F.idPartido
			    AND partidos.Local = equipoLocal.idEquipo
			    AND partidos.Visitante = equipoVisitante.idEquipo
			    AND partidos.computado = 0";

	$result = mysql_query($sql, $link);

	$sql = "INSERT INTO puntos 
			SELECT 
				apuesta1F.idLogin, 
				200,
			    CONCAT('Acierto ganador y Diferencia de goles Partido: ', equipoLocal.Nombre, ' vs ', equipoVisitante.Nombre)
			FROM 
				apuesta1F, 
			    resultados,
			    partidos,
			    equipos AS equipoLocal,
			    equipos AS equipoVisitante
			WHERE
				apuesta1F.idPartido = resultados.idPartido
			    AND apuesta1F.diferenciaGoles = resultados.diferenciaGoles
				AND apuesta1F.ganador = resultados.ganador
			    AND partidos.idPartido = apuesta1F.idPartido
			    AND partidos.Local = equipoLocal.idEquipo
			    AND partidos.Visitante = equipoVisitante.idEquipo
			    AND partidos.computado = 0";

	$result = mysql_query($sql, $link);

	$sql = "INSERT INTO puntos 
			SELECT 
				apuesta1F.idLogin, 
				100,
			    CONCAT('Acierto ganador Partido: ', equipoLocal.Nombre, ' vs ', equipoVisitante.Nombre)
			FROM 
				apuesta1F, 
			    resultados,
			    partidos,
			    equipos AS equipoLocal,
			    equipos AS equipoVisitante
			WHERE
				apuesta1F.idPartido = resultados.idPartido
			    AND apuesta1F.ganador = resultados.ganador
			    AND partidos.idPartido = apuesta1F.idPartido
			    AND partidos.Local = equipoLocal.idEquipo
			    AND partidos.Visitante = equipoVisitante.idEquipo
			    AND partidos.computado = 0";

	$result = mysql_query($sql, $link);
	
	$sql = "UPDATE partidos SET computado = 1 WHERE idPartido IN (SELECT idPartido FROM resultados);";
	$result = mysql_query($sql, $link);
?>