<?php 
   include("conectar.php"); 
	$link=Conectarse();
	
	$Regional = $_POST['Regional'];
	$Area = $_POST['Area'];

	if ($Regional != 7)
	{
		$Regional = " AND Regional.idRegional = $Regional ";
	} else
	{
		$Regional = "";
	}

	if ($Area != 1)
	{
		$Area = " AND Area.idArea = $Area ";
	} else
	{
		$Area = "";
	}
	$Index = 0;	
	
	class Reporte
	{
		public $Usuario;
		public $Regional;
		public $Area;
		public $Preguntas;
		public $Puntaje;
		public $UltimaPregunta;
		
	}

	$sql = "SELECT 
			Usuario.Nombre AS 'Usuario',
	        Regional.Nombre AS 'Regional',
	        Area.Nombre AS 'Area',
	        COUNT(Test.idTest) AS 'Preguntas',
	        SUM(Test.Resultado) AS 'Puntaje',
	        MAX(Test.Fecha) AS 'UltimaPregunta'
		FROM 
			Login_has_Test AS Test,
	        DatosUsuarios AS Usuario,
	        Regionales AS Regional,
	        Areas AS Area
		WHERE
			Test.idLogin = Usuario.idLogin
	        AND Usuario.idRegional = Regional.idRegional
	        AND Usuario.idArea = Area.idArea
	        $Regional
	        $Area
		GROUP BY
			Usuario.Nombre,
	        Regional.Nombre,
	        Area.Nombre";
			
	$result = mysql_query($sql, $link);
	
	 while($row = mysql_fetch_array($result))
	{ 
		$Reportes[$Index] = new Reporte();

		$Reportes[$Index]->Usuario = utf8_encode($row['Usuario']);
		$Reportes[$Index]->Regional = utf8_encode($row['Regional']);
		$Reportes[$Index]->Area = utf8_encode($row['Area']);
		$Reportes[$Index]->Preguntas = utf8_encode($row['Preguntas']);
		$Reportes[$Index]->Puntaje = utf8_encode($row['Puntaje']);
		$Reportes[$Index]->UltimaPregunta = utf8_encode($row['UltimaPregunta']);
	
		$Index++;	
	}

	if ($Index > 0)
	{
		echo json_encode($Reportes);	
	} else
	{
		echo 0;
	}


		
	mysql_close($link);	
	
?> 
