<?php 
   include("conectar.php"); 
	$link=Conectarse();
	
	$Desde = $_POST['Desde'];
	$Hasta = $_POST['Hasta'];
	$Regional = $_POST['Regional'];
	$Area = $_POST['Area'];
	$Parametros = $_POST['Parametros'];
	$pParametros = str_replace(", ", ", ' ', ", $Parametros);

	if ($Desde <> "")
	{
		$Desde = "AND Reporte.Fecha > '$Desde'";
	}
	if ($Hasta <> "")
	{
		$Hasta = "AND Reporte.Fecha < '$Hasta 23:59:59'";	
	}

	if ($Regional != 7)
	{
		$Regional = " AND Regional.idRegional = $Regional ";
	} else
	{
		$Regional = "";
	}

	if ($Area != 1)
	{
		$Area = " AND AreaCierre.idArea = $Area ";
	} else
	{
		$Area = "";
	}

	//$Parametros = "Mes, ' ', DiaSem";
	$Index = 0;	
	
	class Reporte
	{
		public $Cantidad;
		public $label;
	}

	$sql = "SELECT
				CONCAT($pParametros) AS 'Parametros',
				COUNT(*) AS Cantidad
			FROM (
			SELECT
				Reporte.idRep1 AS 'Consecutivo',
				DATE_FORMAT(Reporte.Fecha, '%Y-%d-%m') AS 'Fecha',
				DATE_FORMAT(Reporte.Fecha, '%M') AS 'Mes',
				DATE_FORMAT(Reporte.Fecha, '%d') AS 'Dia',
				DATE_FORMAT(Reporte.Fecha, '%W') AS 'DiaSem',
		        Reporte.Nombre AS 'Nombre',
		        Reporte.Cargo AS 'Cargo',
		        Regional.Nombre AS 'Regional',
		        Area.Nombre AS 'Area',
		        Departamento.Nombre AS 'Departamento',
		        Municipio.Nombre AS 'Municipio',
		        Proyecto.Nombre AS 'Proyecto',
		        Item.Nombre AS 'Item',
		        SubItem.Nombre AS 'SubItem',
		        AreaCierre.Nombre AS 'AreaResponsable',
		        Causa.Nombre AS 'CausaRaiz',
		        Reporte.Calificacion AS 'Tarjeta',
		        Usuario.Nombre AS 'Usuario',
		        Estado.Nombre AS 'Estado'
			FROM
				Rep1 AS Reporte,
		        Rep1_ActosCondiciones AS Item,
		        Rep1_SubItems AS SubItem,
		        DatosUsuarios AS Usuario,
		        Municipios AS Municipio,
		        Departamentos AS Departamento,
		        Proyectos AS Proyecto,
		        Regionales AS Regional,
		        Areas AS Area,
		        Areas AS AreaCierre,
		        CausaRaiz AS Causa,
		        EstadosReporte AS Estado
			WHERE
				Reporte.Item = Item.idRep1_ActosY
		        AND Reporte.SubItem = SubItem.idRep1_SubItems
		        AND Reporte.idLogin = Usuario.idLogin
		        AND Reporte.idMunicipio = Municipio.idMunicipio
		        AND Reporte.idDepartamento = Departamento.idDepartamento
		        AND Reporte.idProyecto = Proyecto.idProyecto
		        AND Reporte.idRegional = Regional.idRegional
		        AND Reporte.idArea = Area.idArea
		        AND Reporte.idAreaResponsable = AreaCierre.idArea
		        AND Reporte.idCausa = Causa.idCausa
		        AND Reporte.idEstado = Estado.idEstado
		        $Desde $Hasta $Regional $Area
			) AS Datos
			GROUP BY
			$Parametros";

	$result = mysql_query($sql, $link);
	
	 while($row = mysql_fetch_array($result))
	{ 
		$Reportes[$Index] = new Reporte();
		$Reportes[$Index]->Cantidad = $row['Cantidad'];
		$Reportes[$Index]->label = utf8_encode($row['Parametros']);
	
		$Index++;	
	}

	if ($Index > 0)
	{
		echo json_encode($Reportes);	
	} else
	{
		echo 0;
	}
	mysql_close($link);	
?> 
