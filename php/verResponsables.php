<?php

/*
<select id="cboReporte_Responsable" class="inputForm" style="display: none;">
<option value="1">Administrador</option>
<option value="2">HSE</option>
<option value="3">Calidad</option>
<option value="4">Recursos Humanos</option>
<option value="5">TIC</option>
<option value="6">Recursos Físicos</option>
<option value="7">Administrador de Proyecto</option>
<option value="8">Director de Proyecto</option>
</select>

<select id="cboReporte_Regional" class="inputForm" style="display: none;">
<option value="1">Norte</option>
<option value="2">Occidente</option>
<option value="3">Centro</option>
<option value="4">Sur</option>
<option value="5">Oriente</option>
<option value="6">Internacional</option>
<option value="7">Corporativa</option>
</select>
*/

  function verReponsable($idArea, $idRegional)
  {
      $Regional = array();
    //Responsable[Regional][Area]

    for ($i=1; $i < 8; $i++) 
    { 
      $Regional[$i] = array();
    }

    $Regional[1][1] = "Lina.Moreno@wspgroup.com";
    $Regional[1][2] = "Lina.Moreno@wspgroup.com";
    $Regional[1][3] = "Lina.Moreno@wspgroup.com";
    $Regional[1][4] = "Carolina.Palacio@wspgroup.com";
    $Regional[1][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[1][6] = "Angelica.Garzon@wspgroup.com";
    $Regional[1][7] = "Leonardo.Triana@wspgroup.com";
    $Regional[1][8] = "Leonardo.Triana@wspgroup.com";

    $Regional[2][1] = "Paula.Mejia@wspgroup.com";
    $Regional[2][2] = "Paula.Mejia@wspgroup.com";
    $Regional[2][3] = "Paula.Mejia@wspgroup.com";
    $Regional[2][4] = "liliana.arcila@wspgroup.com";
    $Regional[2][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[2][6] = "liliana.arcila@wspgroup.com";
    $Regional[2][7] = "Luis.Perez@wspgroup.com";
    $Regional[2][8] = "Luis.Perez@wspgroup.com";

    $Regional[3][1] = "Ivette.Sanchez@wspgroup.com";
    $Regional[3][2] = "Ivette.Sanchez@wspgroup.com";
    $Regional[3][3] = "Ivette.Sanchez@wspgroup.com";
    $Regional[3][4] = "Oscar.Bohorquez@wspgroup.com";
    $Regional[3][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[3][6] = "Claudia.Romero@wspgroup.com";
    $Regional[3][7] = "Jorge.Duarte@wspgroup.com";
    $Regional[3][8] = "Jorge.Duarte@wspgroup.com";

    $Regional[4][1] = "Mabel.Miranda@wspgroup.com";
    $Regional[4][2] = "Mabel.Miranda@wspgroup.com";
    $Regional[4][3] = "Mabel.Miranda@wspgroup.com";
    $Regional[4][4] = "Victoria.Coronado@wspgroup.com";
    $Regional[4][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[4][6] = "David.Erazo@wspgroup.com";
    $Regional[4][7] = "Cesar.Zapata@wspgroup.com";
    $Regional[4][8] = "Cesar.Zapata@wspgroup.com";

    $Regional[5][1] = "Natali.Valdes@wspgroup.com";
    $Regional[5][2] = "Natali.Valdes@wspgroup.com";
    $Regional[5][3] = "Natali.Valdes@wspgroup.com";
    $Regional[5][4] = "Natali.Valdes@wspgroup.comcom";
    $Regional[5][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[5][6] = "Natali.Valdes@wspgroup.com";
    $Regional[5][7] = "nicolas.castano@wspgroup.com";
    $Regional[5][8] = "nicolas.castano@wspgroup.com";

    $Regional[6][1] = "Ivette.Sanchez@wspgroup.com";
    $Regional[6][2] = "Ivette.Sanchez@wspgroup.com";
    $Regional[6][3] = "Ivette.Sanchez@wspgroup.com";
    $Regional[6][4] = "Oscar.Bohorquez@wspgroup.com";
    $Regional[6][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[6][6] = "Claudia.Romero@wspgroup.com";
    $Regional[6][7] = "Jorge.Duarte@wspgroup.com";
    $Regional[6][8] = "Jorge.Duarte@wspgroup.com";

    $Regional[7][1] = "Ivette.Sanchez@wspgroup.com";
    $Regional[7][2] = "Ivette.Sanchez@wspgroup.com";
    $Regional[7][3] = "Ivette.Sanchez@wspgroup.com";
    $Regional[7][4] = "Oscar.Bohorquez@wspgroup.com";
    $Regional[7][5] = "ITColombia.Servicedesk@wspgroup.com";
    $Regional[7][6] = "Claudia.Romero@wspgroup.com";
    $Regional[7][7] = "Jorge.Duarte@wspgroup.com";
    $Regional[7][8] = "Jorge.Duarte@wspgroup.com";

    return $Regional[$idRegional][$idArea];
  }
?>