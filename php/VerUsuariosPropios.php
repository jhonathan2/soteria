<?php 
   include("conectar.php"); 
	$link=Conectarse(); 
	
	$i = 0;
	$Index = 0;	
	$Id = addslashes($_POST['Id']);
	
	class User
	{
		public $IdUser;
		public $UserName;
		public $Name;
		public $NickName;
		public $Mail;
		public $Phone;
		public $Owner;
		public $IdCompany;
		public $Company;
		public $Empresa;
		public $State;
		public $IdInitialRol;
		public $RolName;
	}

	$sql = "SELECT DISTINCT
				l.IdLogin AS 'Id', 
				l.Usuario AS 'UserName',
				d.Nombre AS 'Name',
				d.NickName AS 'NickName',
				d.Correo AS 'Mail', 
				o.Nombre AS 'Owner', 
				c.Nombre AS 'Company',
				c.IdDepartamento AS 'IdCompany',
				em.Em_Nombre AS 'Empresa',
				l.Estado as 'State',
				d.IdInitialRoll AS 'IdInitialRol',
				p.Nombre AS 'RolName'
		FROM
				Login AS l, 
				DatosUsuarios AS d,
				DatosUsuarios AS o,
				Departamento AS c,
				Rol AS p, 
				Transacciones AS r,
				Empresas AS em
		WHERE
			l.IdLogin = d.IdUsersData AND 
			d.IdDepartamento = c.IdDepartamento AND
			p.idRol = d.IdInitialRoll AND
			r.IdUsuarioMaestro = o.IdUsersData AND
			r.IdUsuario = d.IdUsersData AND
			d.IdEmpresa = em.Em_idEmpresa AND
			r.IdUsuarioMaestro = '$Id';";
			
	
	$result = mysql_query($sql, $link);
	$row = mysql_fetch_array($result);
	
	do
	{ 
		$Users[$Index] = new User();
		
		$Users[$Index]->IdUser = $row['Id'];
		$Users[$Index]->UserName = utf8_encode($row['UserName']);
		$Users[$Index]->Name = utf8_encode($row['Name']);
		$Users[$Index]->NickName = utf8_encode($row['NickName']);
		$Users[$Index]->Empresa = utf8_encode($row['Empresa']);
		$Users[$Index]->Mail = utf8_encode($row['Mail']);
		$Users[$Index]->Owner = utf8_encode($row['Owner']);
		$Users[$Index]->IdCompany = utf8_encode($row['IdCompany']);
		$Users[$Index]->Company = utf8_encode($row['Company']);
		$Users[$Index]->State = utf8_encode($row['State']);
		$Users[$Index]->IdInitialRol = utf8_encode($row['IdInitialRol']);
		$Users[$Index]->RolName = utf8_encode($row['RolName']);

		$Index++;	
		
	} while($row = mysql_fetch_array($result));

		
	mysql_close($link);	
	echo json_encode($Users);
?> 
