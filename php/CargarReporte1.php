<?php 
   include("conectar.php"); 
	$link=Conectarse();
	
	$Desde = $_POST['Desde'];
	$Hasta = $_POST['Hasta'];
	$Regional = $_POST['Regional'];
	$Area = $_POST['Area'];

	if ($Desde <> "")
	{
		$Desde = " AND Reporte.Fecha >= '$Desde 00:00:00'";
	}
	if ($Hasta <> "")
	{
		$Hasta = " AND Reporte.Fecha <= '$Hasta 23:59:59'";	
	}

	if ($Regional != 7)
	{
		$Regional = " AND Regional.idRegional = $Regional ";
	} else
	{
		$Regional = "";
	}

	if ($Area != 1)
	{
		$Area = " AND AreaCierre.idArea = $Area ";
	} else
	{
		$Area = "";
	}


	$Index = 0;	
	
	class Reporte
	{
		public $Consecutivo;
		public $Fecha;
		public $Nombre;
		public $Cargo;
		public $Regional;
		public $Area;
		public $Departamento;
		public $Municipio;
		public $Proyecto;
		public $Item;
		public $SubItem;
		public $AreaResponsable;
		public $CausaRaiz;
		public $Tarjeta;
		public $Usuario;
		public $Estado;
	}

	$sql = "SELECT
				Reporte.idRep1 AS 'Consecutivo',
				DATE_FORMAT(Reporte.Fecha, '%Y-%m-%d') AS 'Fecha',
		        Reporte.Nombre AS 'Nombre',
		        Reporte.Cargo AS 'Cargo',
		        Regional.Nombre AS 'Regional',
		        Area.Nombre AS 'Area',
		        Departamento.Nombre AS 'Departamento',
		        Municipio.Nombre AS 'Municipio',
		        Proyecto.Nombre AS 'Proyecto',
		        Item.Nombre AS 'Item',
		        SubItem.Nombre AS 'SubItem',
		        AreaCierre.Nombre AS 'AreaResponsable',
		        Causa.Nombre AS 'CausaRaiz',
		        Reporte.Calificacion AS 'Tarjeta',
		        Usuario.Nombre AS 'Usuario',
		        Estado.Nombre AS 'Estado'
			FROM
				Rep1 AS Reporte,
		        Rep1_ActosCondiciones AS Item,
		        Rep1_SubItems AS SubItem,
		        DatosUsuarios AS Usuario,
		        Municipios AS Municipio,
		        Departamentos AS Departamento,
		        Proyectos AS Proyecto,
		        Regionales AS Regional,
		        Areas AS Area,
		        Areas AS AreaCierre,
		        CausaRaiz AS Causa,
		        EstadosReporte AS Estado
			WHERE
				Reporte.Item = Item.idRep1_ActosY
		        AND Reporte.SubItem = SubItem.idRep1_SubItems
		        AND Reporte.idLogin = Usuario.idLogin
		        AND Reporte.idMunicipio = Municipio.idMunicipio
		        AND Reporte.idDepartamento = Departamento.idDepartamento
		        AND Reporte.idProyecto = Proyecto.idProyecto
		        AND Reporte.idRegional = Regional.idRegional
		        AND Reporte.idArea = Area.idArea
		        AND Reporte.idAreaResponsable = AreaCierre.idArea
		        AND Reporte.idCausa = Causa.idCausa
		        AND Reporte.idEstado = Estado.idEstado
		        $Desde $Hasta $Regional $Area";

			
	$result = mysql_query($sql, $link);
	
	 while($row = mysql_fetch_array($result))
	{ 
		$Reportes[$Index] = new Reporte();
		$Reportes[$Index]->Consecutivo = $row['Consecutivo'];
		$Reportes[$Index]->Fecha = utf8_encode($row['Fecha']);
		$Reportes[$Index]->Nombre = $row['Nombre'];
		$Reportes[$Index]->Cargo = $row['Cargo'];
		$Reportes[$Index]->Regional = utf8_encode($row['Regional']);
		$Reportes[$Index]->Area = utf8_encode($row['Area']);
		$Reportes[$Index]->Departamento = utf8_encode($row['Departamento']);
		$Reportes[$Index]->Municipio = utf8_encode($row['Municipio']);
		$Reportes[$Index]->Proyecto = utf8_encode($row['Proyecto']);
		$Reportes[$Index]->Item = utf8_encode($row['Item']);
		$Reportes[$Index]->SubItem = utf8_encode($row['SubItem']);
		$Reportes[$Index]->AreaResponsable = utf8_encode($row['AreaResponsable']);
		$Reportes[$Index]->CausaRaiz = utf8_encode($row['CausaRaiz']);
		$Reportes[$Index]->Tarjeta = utf8_encode($row['Tarjeta']);
		$Reportes[$Index]->Usuario = utf8_encode($row['Usuario']);
		$Reportes[$Index]->Estado = utf8_encode($row['Estado']);
	
		$Index++;	
	}

	if ($Index > 0)
	{
		echo json_encode($Reportes);	
	} else
	{
		echo 0;
	}
	mysql_close($link);	
?> 
