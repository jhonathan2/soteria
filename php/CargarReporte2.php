<?php 
   include("conectar.php"); 
	$link=Conectarse();
	
	$Desde = $_POST['Desde'];
	$Hasta = $_POST['Hasta'];
	$Regional = $_POST['Regional'];
	$Area = $_POST['Area'];

	if ($Desde <> "")
	{
		$Desde = " AND Reporte.Fecha >= '$Desde 00:00:00'";
	}
	if ($Hasta <> "")
	{
		$Hasta = " AND Reporte.Fecha <= '$Hasta 23:59:59'";	
	}

	if ($Regional != 7)
	{
		$Regional = " AND Regional.idRegional = $Regional ";
	} else
	{
		$Regional = "";
	}

	if ($Area != 1)
	{
		$Area = " AND AreaCierre.idArea = $Area ";
	} else
	{
		$Area = "";
	}


	$Index = 0;	
	
	class Reporte
	{
		public $Consecutivo;
		public $Fecha;
		public $Nombre;
		public $Cedula;
		public $Cargo;
		public $Regional;
		public $Area;
		public $Departamento;
		public $Municipio;
		public $Proyecto;
		public $Descripcion;
		public $Estados;
		public $Errores;
		public $Consecuencias;
		public $Prevenciones;
		public $Usuario;
	}

	$sql = "SELECT
				Reporte.idActo AS 'Consecutivo',
				DATE_FORMAT(Reporte.Fecha, '%Y-%m-%d') AS 'Fecha',
		        Reporte.Nombre AS 'Nombre',
		        Reporte.Cedula AS 'Cedula',
		        Reporte.Cargo AS 'Cargo',
		        Regional.Nombre AS 'Regional',
		        Area.Nombre AS 'Area',
		        Departamento.Nombre AS 'Departamento',
		        Municipio.Nombre AS 'Municipio',
		        Proyecto.Nombre AS 'Proyecto',
		        Reporte.Descripcion AS 'Descripcion',
		        Reporte.Estados AS 'Estados',
		        Reporte.Errores AS 'Errores',
		        Reporte.Consecuencia AS 'Consecuencias',
		        Reporte.Prevencion AS 'Prevenciones',
		        Usuario.Nombre AS 'Usuario'
			FROM
				Actos AS Reporte,
		        DatosUsuarios AS Usuario,
		        Municipios AS Municipio,
		        Departamentos AS Departamento,
		        Proyectos AS Proyecto,
		        Regionales AS Regional,
		        Areas AS Area
			WHERE
				Reporte.idLogin = Usuario.idLogin
		        AND Reporte.idMunicipio = Municipio.idMunicipio
		        AND Reporte.idDepartamento = Departamento.idDepartamento
		        AND Reporte.idProyecto = Proyecto.idProyecto
		        AND Reporte.idRegional = Regional.idRegional
		        AND Reporte.idArea = Area.idArea
		        $Desde $Hasta $Regional $Area";

			
	$result = mysql_query($sql, $link);
	
	 while($row = mysql_fetch_array($result))
	{ 
		$Reportes[$Index] = new Reporte();
		$Reportes[$Index]->Consecutivo = $row['Consecutivo'];
		$Reportes[$Index]->Fecha = utf8_encode($row['Fecha']);
		$Reportes[$Index]->Nombre = $row['Nombre'];
		$Reportes[$Index]->Cedula = $row['Cedula'];
		$Reportes[$Index]->Cargo = $row['Cargo'];
		$Reportes[$Index]->Regional = utf8_encode($row['Regional']);
		$Reportes[$Index]->Area = utf8_encode($row['Area']);
		$Reportes[$Index]->Departamento = utf8_encode($row['Departamento']);
		$Reportes[$Index]->Municipio = utf8_encode($row['Municipio']);
		$Reportes[$Index]->Proyecto = utf8_encode($row['Proyecto']);
		$Reportes[$Index]->Descripcion = utf8_encode($row['Descripcion']);
		$Reportes[$Index]->Estados = utf8_encode($row['Estados']);
		$Reportes[$Index]->Errores = utf8_encode($row['Errores']);
		$Reportes[$Index]->Consecuencias = utf8_encode($row['Consecuencias']);
		$Reportes[$Index]->Prevenciones = utf8_encode($row['Prevenciones']);
		$Reportes[$Index]->Usuario = utf8_encode($row['Usuario']);
	
		$Index++;	
	}

	if ($Index > 0)
	{
		echo json_encode($Reportes);	
	} else
	{
		echo 0;
	}
	mysql_close($link);	
?> 
