﻿ $(document).on("ready", arranque);

function arranque()
{
	$("#frmLogin").on('submit', frmLogin_Submit);
}
function frmLogin_Submit (argument) 
{
	argument.preventDefault();
	$.post("php/RecuperarClaveCorreo.php",
	{
		Username: $("#txtUsername").val(),
		Correo: $("#txtCorreo").val()
	},
	function (data) 
	{
	 	if (data == 0) //El Usuario no fue encontrado
	 	{
	 		$("#artResultado").html("El usuario no ha sido encontrado.")
	 	}
	 	else if (data == 1) //No hay un correo registrado
	 	{
	 		$("#artResultado").html("El usuario no tienen ningún correo registrado, por favor contacte al administrador.")
	 	}
	 	else //¿Json?
	 	{
	 		var mensaje;
	 		mensaje = "Gracias " + data.NickName;
	 		mensaje += "<br /> Un mensaje ha sido enviado a: " + data.Correo;
	 		mensaje += "<br /> por favor revise su bandeja.";
	 		$("#artResultado").html(mensaje);
	 	}

	}, "json");

}