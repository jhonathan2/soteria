SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `Login`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Login` (
  `idLogin` INT NOT NULL AUTO_INCREMENT ,
  `Usuario` VARCHAR(45) NOT NULL ,
  `Clave` VARCHAR(45) NOT NULL ,
  `Estado` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idLogin`) )
ENGINE = InnoDB
COMMENT = 'Almacena los datos de Inicio de Sesión de Cada Usuario\n';


-- -----------------------------------------------------
-- Table `Roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Roles` (
  `idRol` INT NOT NULL AUTO_INCREMENT ,
  `Nombre` VARCHAR(45) NULL ,
  `Descripcion` VARCHAR(255) NULL ,
  PRIMARY KEY (`idRol`) )
ENGINE = InnoDB
COMMENT = 'Contiene el listado de Roles disponibles en el Sistema';


-- -----------------------------------------------------
-- Table `DatosUsuarios`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DatosUsuarios` (
  `idLogin` INT NOT NULL ,
  `Nombre` VARCHAR(120) NOT NULL ,
  `Correo` VARCHAR(80) NOT NULL ,
  `Roles_idRol` INT NOT NULL ,
  INDEX `fk_table1_Login` (`idLogin` ASC) ,
  PRIMARY KEY (`idLogin`, `Roles_idRol`) ,
  INDEX `fk_DatosUsuarios_Roles1` (`Roles_idRol` ASC) ,
  UNIQUE INDEX `Correo_UNIQUE` (`Correo` ASC) ,
  CONSTRAINT `fk_table1_Login`
    FOREIGN KEY (`idLogin` )
    REFERENCES `Login` (`idLogin` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DatosUsuarios_Roles1`
    FOREIGN KEY (`Roles_idRol` )
    REFERENCES `Roles` (`idRol` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Contiene información adicional del Usuario\n';


-- -----------------------------------------------------
-- Table `Funciones`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Funciones` (
  `idFuncion` INT NOT NULL AUTO_INCREMENT ,
  `Nombre` VARCHAR(45) NULL ,
  `ControlAsociado` VARCHAR(45) NOT NULL COMMENT 'El ID en el Documento HTML que le permite acceder a una Función' ,
  `Descripcion` VARCHAR(255) NULL ,
  PRIMARY KEY (`idFuncion`) )
ENGINE = InnoDB
COMMENT = 'Contiene las funciones que pueden asociarse a cada rol';


-- -----------------------------------------------------
-- Table `Roles_has_Funciones`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Roles_has_Funciones` (
  `Roles_idRol` INT NOT NULL ,
  `Funciones_idFuncion` INT NOT NULL ,
  PRIMARY KEY (`Roles_idRol`, `Funciones_idFuncion`) ,
  INDEX `fk_Roles_has_Funciones_Funciones1` (`Funciones_idFuncion` ASC) ,
  INDEX `fk_Roles_has_Funciones_Roles1` (`Roles_idRol` ASC) ,
  CONSTRAINT `fk_Roles_has_Funciones_Roles1`
    FOREIGN KEY (`Roles_idRol` )
    REFERENCES `Roles` (`idRol` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Roles_has_Funciones_Funciones1`
    FOREIGN KEY (`Funciones_idFuncion` )
    REFERENCES `Funciones` (`idFuncion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Rep1_ActosCondiciones`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Rep1_ActosCondiciones` (
  `idRep1_ActosY` INT NOT NULL ,
  `Nombre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRep1_ActosY`) )
ENGINE = InnoDB
COMMENT = 'Almacena los Actos y condiciones Subestandar';


-- -----------------------------------------------------
-- Table `Rep1_SubItems`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Rep1_SubItems` (
  `idRep1_SubItems` INT NOT NULL AUTO_INCREMENT ,
  `Codigo` VARCHAR(7) NOT NULL ,
  `Nombre` VARCHAR(7) NULL ,
  `Rep1_ActosCondiciones_idRep1_ActosY` INT NOT NULL ,
  PRIMARY KEY (`idRep1_SubItems`) ,
  INDEX `fk_Rep1_SubItems_Rep1_ActosCondiciones1` (`Rep1_ActosCondiciones_idRep1_ActosY` ASC) ,
  CONSTRAINT `fk_Rep1_SubItems_Rep1_ActosCondiciones1`
    FOREIGN KEY (`Rep1_ActosCondiciones_idRep1_ActosY` )
    REFERENCES `Rep1_ActosCondiciones` (`idRep1_ActosY` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Contiene los subitems de cada Acto y Codicion Subestandar\n';


-- -----------------------------------------------------
-- Table `Rep1`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Rep1` (
  `idRep1` INT NOT NULL AUTO_INCREMENT ,
  `Nombre` VARCHAR(80) NOT NULL ,
  `Fecha` DATETIME NOT NULL ,
  `Cargo` VARCHAR(45) NULL ,
  `Sitio` VARCHAR(45) NULL ,
  `Descripcion` LONGTEXT NULL ,
  `Correctivos` LONGTEXT NULL ,
  `Rep1_SubItems_idRep1_SubItems` INT NOT NULL ,
  `Login_idLogin` INT NOT NULL ,
  PRIMARY KEY (`idRep1`) ,
  INDEX `fk_Rep1_Rep1_SubItems1` (`Rep1_SubItems_idRep1_SubItems` ASC) ,
  INDEX `fk_Rep1_Login1` (`Login_idLogin` ASC) ,
  CONSTRAINT `fk_Rep1_Rep1_SubItems1`
    FOREIGN KEY (`Rep1_SubItems_idRep1_SubItems` )
    REFERENCES `Rep1_SubItems` (`idRep1_SubItems` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rep1_Login1`
    FOREIGN KEY (`Login_idLogin` )
    REFERENCES `Login` (`idLogin` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Almacena los datos diligenciados en el Reporte de Actos Y Co' /* comment truncated */;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
