﻿var pRegional = 0;
var pArea = 0;
var pCorreo = 0;
var pConsecutivo = 0;
var pEvento;
$(document).on("ready", arranque);

function arranque()
{
	$("#frmLogin").on("submit", frmLogin_Submit)
	$("#btnLogin").button();
	$(".aOcultar").on("click", aOcultar_Click);

	$("#Usuarios_Actualizar").on("submit", ActualizarDatos);
	

	$("#frmCrearUsuarioF").on("submit", frmCrearUsuarioF_Submit);
	$("#lnkCrearUsuario").on("click", lnkCrearUsuario_Click);

	$(".aTerminos").on('click', aTerminos_Click);

	CargarAreas($("#cboArea"), 0);
		$("#cboArea").combobox();
	CargarAreas($("#txtUsuarios_Actualizar_Area"), 0);

	CargarRegionales($("#cboRegional"));
		$("#cboRegional").combobox();
	CargarRegionales($("#txtUsuarios_Actualizar_Regional"));

		$("#txtCorreo").on('change', txtCorreo_change);

		$("#txtUsuario").on('change', txtUsuario_Change);
	
	if(localStorage.UsuarioWSPSISO)
	{window.location.replace("home.html");}

	

}
function frmLogin_Submit(evento)
{
	evento.preventDefault();  
	$.post("php/ValidarUsuario.php",  
		{
			Usuario: $("#txtUsername").val(), 
			Clave: $("#txtPassword").val()
		}, 
		function(data)
		{	
			if (data.Id)
			{
				var tmpId = data.Id;
				var tmpCorreo = data.Correo.split("@");
				$("#txtUsuarios_Actualizar_Usuario span").text(data.Nombre);
				if (tmpCorreo[0] == "Pendiente")
				{
					pConsecutivo = data.Id;
					pEvento = evento;
					$("#Usuarios_Actualizar").dialog({
							autoOpen: false, 				
							closeOnEscape: true,
							minWidth: 600,
							modal: true,
							title: "Actualización de Datos"});
					$("#Usuarios_Actualizar").dialog('open');
				} else
				{
					localStorage.setItem("UsuarioWSPSISO", '[' + JSON.stringify(data) + ']');			
					window.location.replace("home.html");	
				}
				
			}
			else
			{
				$("#alertEstadoLogin").fadeIn(200).delay(1600).fadeOut(1200);
			}
		}, 
		"json");	
}
(function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
          	change: function(event, ui)
          	{

          		var idElemento = $(ui.item.option.parentElement).attr("id");

          		if (idElemento == "cboArea")
	          		{
	          			pArea = ui.item.option.value;;
	          		}
	          	if (idElemento == "cboRegional")
		          	{
		          		pRegional = ui.item.option.value;
		          	}
          	},
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          });
 
        
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Mostrar Todos" )
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );

 function CargarAreas(obj, pSac)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarAreas.php", {SAC: pSac}, function(data)
		{
			var tds = "";
			$.each(data, function(index, value)
				{
					tds = "<option value='" + value.idArea + "'>" + value.Nombre + "</option>";
					$(obj).append(tds);
				});
		}
		, "json");
}
function CargarRegionales(obj)
{
	var objOption = $(obj).find("option");
	$(objOption).remove();

	$.post("php/CargarRegionales.php", {}, function(data)
		{
			var tds = "";
			$.each(data, function(index, value)
				{
					tds = "<option value='" + value.idRegional + "'>" + value.Nombre + "</option>";
					$(obj).append(tds);
				});
		}
		, "json");
}
function txtCorreo_change()
{
	var tmpCorreo = $("#txtCorreo").val().split("@");
	if (typeof(tmpCorreo[1]) != "undefined")
	{
		if (tmpCorreo[1] != "wspgroup.com")
		{
			$("#ValidarCorreo").slideDown();
			pCorreo = 0;
		} else
		{
			$("#ValidarCorreo").slideUp();
			pCorreo = 1;
		}
	} 
}
function frmCrearUsuarioF_Submit(argumento)
{
	argumento.preventDefault();
	if ($("#txtUsuario").val != "")
	{
		if (pRegional > 0)
		{
			if (pArea > 0)
			{
				if ($("#txtCorreo").val() != "" && pCorreo > 0)
				{
					if ($("#txtNombre").val() != "")
					{
						$.post("php/CrearUsuario.php", 
						{
							Usuario: $("#txtUsuario").val(),
							Correo: $("#txtCorreo").val(),
							Rol : 2,
							Nombre: $("#txtNombre").val(),
							Regional: pRegional,
							Area: pArea
						}, function(data)
						{
							console.log(data);
							if (parseInt(data) > 0)
							{
								MostrarAlerta("default", "ui-icon-check", "hey!", "Usuario Creado, por favor revise su correo");					
								$("#txtUsuario, #txtCorreo, #txtNombre").val("");

							}else
							{
								MostrarAlerta("error", "ui-icon-alert", "Error!", data);
							}
						});
					} else
					{
						MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Nombre Completo");				
					}
				} else
				{
					MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Correo");		
				}
			} else
			{
				MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Área");
			}
		} else
		{
			MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar la Regional");
		}

	} else
	{
		MostrarAlerta("error", "ui-icon-alert", "Error!", "Falta diligenciar el Usuario");
	}
}
function MostrarAlerta(TipoMensaje, Icono, Strong, Mensaje)
{
	/*NombreContenedor : Id del Div que contiene el MessageAlert
	 * TipoMensaje : {highlight, error, default}
	 * Icono : Icono que acompaña el mensaje ver listado en bootstrap
	 * Mensaje del AlertMessage*/
	 
	$("#alertMensaje").removeClass(function() {return $(this).prev().attr('class');});
	$("#alertMensaje").addClass("Alerta");
	$("#alertMensaje span").removeClass("*");
	$("#alertMensaje").addClass("ui-state-" + TipoMensaje);
	$("#alertMensaje span").addClass(Icono);
	$("#alertMensaje strong").text(Strong);
	$("#alertMensaje texto").text(Mensaje);
	
	$("#alertMensaje").fadeIn(300).delay(2600).fadeOut(600);
}
function aOcultar_Click(argumento)
{
	argumento.preventDefault();
	$("#frmCrearUsuario").hide('slide');
	$("#frmLogin").slideDown();
	
}
function lnkCrearUsuario_Click()
{
	$("#frmLogin").slideUp();
	$("#frmCrearUsuario").show('slide');	
}
function txtUsuario_Change()
{
	$("#txtUsuario").val($("#txtUsuario").val().replace(/ /gi, ""));
	$("#txtUsuario").val($("#txtUsuario").val().replace(/ñ/gi, "n"));
	$("#txtUsuario").val($("#txtUsuario").val().toLowerCase());
}

function aTerminos_Click()
{
	$("#tTerminos").dialog({
					autoOpen: false, 				
					closeOnEscape: true,
					minWidth: 600,
					modal: true,
					title: "Términos y Condiciones"});
	$("#tTerminos").dialog('open');
}
function ActualizarDatos(evento)
{
	evento.preventDefault();
	$.post("php/ActualizarDatos.php", 
		{
			Consecutivo : pConsecutivo,
			Correo : $("#txtUsuarios_Actualizar_Correo").val(),
			Regional : $("#txtUsuarios_Actualizar_Regional").val(),
			Area : $("#txtUsuarios_Actualizar_Area").val()
		}, function(data)
		{
			$("php/RecuperarClaveCorreo.php", 
				{
					Username: $("#txtUsername").val(),
					Correo: $("#txtUsuarios_Actualizar_Correo").val()
				});
			alert('Se ha enviado un correo a la cuenta ' + $("#txtUsuarios_Actualizar_Correo").val() + ' Para cambiar la clave de acceso');
			$("#Usuarios_Actualizar").dialog('close');
		});
}